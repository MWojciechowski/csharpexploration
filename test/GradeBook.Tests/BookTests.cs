using System;
using Xunit;

namespace GradeBook.Tests
{
    public class BookTests
    {
        [Fact]
        public void BookStatisticsClassesTest()
        {
            // Arrange
            InMemoryBook book = new InMemoryBook("test");
            book.AddGrade(4);
            book.AddGrade(5);
            book.AddGrade(4.0);
            book.AddGrade(2.0);

            // Wrong one, should not be added:
            book.AddGrade(7.3);
            
            // Act
            var result = book.GetStatistics();
            
            var expectedAverage = 15.0 / 4;
            var expectedHigh = 5.0;
            var expectedLow = 2.0;
            var expectedLetter = "B";

            // Assert
            Assert.Equal(expectedAverage, result.Average, 1);
            Assert.Equal(expectedHigh, result.High, 1);
            Assert.Equal(expectedLow, result.Low, 1);
            Assert.Equal(expectedLetter, result.Letter);
        }
    }
}
