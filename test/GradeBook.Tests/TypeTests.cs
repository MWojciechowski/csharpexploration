using System;
using Xunit;

namespace GradeBook.Tests
{
    public delegate string WriteLogDelegate(string logMessage);

    public class TypeTests
    {
        int count = 0;

        [Fact]
        public void WriteLogDelegateCanPointToMethod() {
            // Delegate def
            WriteLogDelegate log = ReturnMessage;

            log += ReturnMessageToLower;
            log += ReturnMessage;

            var result = log("Hello!");
            Assert.Equal(3, count);
        }

        string ReturnMessage(string msg) {
            count++;
            return msg;
        }

        string ReturnMessageToLower(string msg) {
            count++;
            return msg;
        }

        [Fact]
        public void GetBookReturnsDifferentObjects()
        {
            InMemoryBook book1 = GetBook("Book 1");
            InMemoryBook book2 = GetBook("Book 2");

            Assert.Equal("Book 1", book1.Name);
            Assert.Equal("Book 2", book2.Name);
            Assert.NotSame(book1, book2);
        }

        [Fact]
        public void GetBooksReferencesSameObjects()
        {
            InMemoryBook book1 = GetBook("Book 1");
            InMemoryBook book2 = book1;

            Assert.Equal("Book 1", book1.Name);
            Assert.Equal("Book 1", book2.Name);
            Assert.Same(book1, book2);
        }

        [Fact]
        public void ChangeBooksNameByReferenceObjects()
        {
            InMemoryBook book1 = GetBook("Book 1");
            SetName(book1, "New Name");

            Assert.Equal("New Name", book1.Name);
        }

        [Fact]
        public void CSharpCanPassByValue()
        {
            InMemoryBook book1 = GetBook("Book 1");
            GetBookSetNameByValue(book1, "New Name");

            Assert.Equal("Book 1", book1.Name);
        }

        [Fact]
        public void CSharpCanPassByReference()
        {
            InMemoryBook book1 = GetBook("Book 1");
            GetBookSetNameByRef(ref book1, "New Name");

            Assert.Equal("New Name", book1.Name);
        }

        [Fact]
        public void Test1()
        {
            var x = GetInt();
            SetIntRef(ref x);

            Assert.Equal(42, x);
        }



        InMemoryBook GetBook(string name)
        {
            return new InMemoryBook(name);
        }

        private void SetName(InMemoryBook book, string name)
        {
            book.Name = name;
        }

        private void GetBookSetNameByValue(InMemoryBook book, string name)
        {
            book = new InMemoryBook(name);
        }

        private void GetBookSetNameByRef(ref InMemoryBook book, string name)
        {
            book = new InMemoryBook(name);
        }

        private int GetInt()
        {
            return 3;
        }

        private void SetIntRef(ref int x)
        {
            x = 42;
        }
    }
}
