using System;
using System.Collections.Generic;

namespace GradeBook
{
    /*Poorly written, but that's how C# fundamentals wanted it*/
    public class Statistics
    {
        public Statistics()
        {
            Average = 0.0;
            High = double.MinValue;
            Low = double.MaxValue;
            Letter = "F";
        }

        public string ConvertGradeToLetterGrade(double grade)
        {
            switch (grade)
            {
                case double g when g > 4.5:
                    return "A";
                case double g when g > 3.5:
                    return "B";
                case double g when g > 3.0:
                    return "C";
                case double g when g > 2.0:
                    return "D";
                default:
                    return "F";
            }
        }

        public double FindLowestValue(List<double> gradesList)
        {
            double lowest = 5.0;
            if (gradesList.Count < 1)
            {
                Console.WriteLine("Provided list of grades is empty. Lowest grade is 0.");
                return lowest = 0;
            }
            foreach (double grade in gradesList)
            {
                if (grade < lowest)
                {
                    lowest = grade;
                }
            }
            Console.WriteLine($"Lowest grade found was {lowest}");
            return lowest;
        }

        public double FindHighestValue(List<double> gradesList)
        {
            double highest = 0.0;
            if (gradesList.Count < 1)
            {
                Console.WriteLine("Provided list of grades is empty. Highest grade is 0.");
                return highest;
            }
            foreach (double grade in gradesList)
            {
                if (grade > highest)
                {
                    highest = grade;
                }
            }
            Console.WriteLine($"Highest grade found was {highest}");
            return highest;
        }

        public double CalculateAverage(List<double> gradesList)
        {
            double average = 0.0;
            if (gradesList.Count < 1)
            {
                Console.WriteLine($"That user seems to not have any grade, average is 0.");
                return average;
            }
            foreach (double grade in gradesList)
            {
                average += grade;
            }
            average /= gradesList.Count;
            Console.WriteLine($"That user has a total of {gradesList.Count} grades, average is {average:N1} grade.");
            return average;
        }
        public double Average { get; set; }
        public double High { get; set; }
        public double Low { get; set; }
        public string Letter { get; set; }
    }
}