using System;
using System.Collections.Generic;

// Can init at runtime/constructor or when declared. Can be dynamic but in constructors scope.
// readonly string science = "Science";

// Can only be declared when initialized, is a implicitly static (by default package protected)
// public const string SCIENCE_NATURE = "Biology";

namespace GradeBook
{
    public delegate void GradeAddedDelegate(object sender, EventArgs args);
    public interface IBook
    {
        void AddGrade(double grade);
        Statistics GetStatistics();
        string Name { get; }
        event GradeAddedDelegate GradeAdded;
    }

    public class NamedObject
    {
        public NamedObject(string name)
        {
            Name = name;
        }
        public string Name { get; set; }
    }
    public abstract class Book : NamedObject, IBook
    {
        public Book(string name) : base(name)
        {
        }
        public abstract event GradeAddedDelegate GradeAdded;
        public abstract void AddGrade(double grade);
        public abstract Statistics GetStatistics();
    }
    public class InMemoryBook : Book
    {
        public InMemoryBook(string name) : base(name)
        {
            Name = name;
            grades = new List<double>();
            Console.WriteLine($"Welcome to {name} gradebook.");
        }

        public override void AddGrade(double grade)
        {
            if (CheckGrade(grade))
            {
                Console.WriteLine($"Added {grade} grade to the book.");
                this.grades.Add(grade);
                if (GradeAdded != null)
                {
                    GradeAdded(this, new EventArgs());
                }
            }
            else
            {
                Console.WriteLine($"Provided: {grade} grade is not in range from 2.0 to 5.0");
            }
        }
        public void AddGrade(string grade)
        {
            Console.WriteLine("Grade letter to number conversion!");
            grade = grade.ToUpper();
            switch (grade)
            {
                case "A":
                    AddGrade(5.0);
                    break;
                case "B":
                    AddGrade(4.0);
                    break;
                case "C":
                    AddGrade(3.5);
                    break;
                case "D":
                    AddGrade(3.0);
                    break;
                case "F":
                    AddGrade(2.0);
                    break;
                default:
                    Console.WriteLine($"Provided: {grade} grade is not in range A,B,C,D,F");
                    break;
            }
        }
        public override event GradeAddedDelegate GradeAdded;
        public void AddGradesList(List<double> gradeList)
        {
            foreach (double grade in gradeList)
            {
                AddGrade(grade);
            }
        }
        public void DeleteGradeById(int id)
        {
            try
            {
                Console.WriteLine($"Removed grade {grades[id]}.");
                this.grades.Remove(grades[id]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void DeleteFirstMetGrade(double grade)
        {
            try
            {
                Console.WriteLine($"Removed grade {grade}.");
                this.grades.Remove(grade);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void ShowAllGrades()
        {
            Console.Write("\nGrades are: { ");
            foreach (double grade in this.grades)
            {
                Console.Write($" {grade} |");
            }
            Console.Write(" };");
            Console.WriteLine("\n");
        }
        public void EditSpecificId(int id, double newValue)
        {
            try
            {
                Console.WriteLine($"Old value: {grades[id]}, changed to {newValue}");
                this.grades[id] = newValue;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void EditFirstMetGrade(double searchedGrade, double newValue)
        {
            try
            {
                Console.WriteLine($"Old value: {searchedGrade}, changed to {newValue}");
                this.grades[this.grades.IndexOf(searchedGrade)] = newValue;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<double> GetGradesList()
        {
            return this.grades;
        }
        public override Statistics GetStatistics()
        {
            Statistics result = new Statistics();
            result.Average = result.CalculateAverage(this.grades);
            result.High = result.FindHighestValue(this.grades);
            result.Low = result.FindLowestValue(this.grades);
            result.Letter = result.ConvertGradeToLetterGrade(result.Average);
            return result;
        }

        private List<double> grades;
        private bool CheckGrade(double grade)
        {
            if (grade >= 2.0 && grade <= 5.0)
            {
                return true;
            }
            return false;
        }
    }
}