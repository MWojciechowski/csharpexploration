﻿using System;
using System.Collections.Generic;

namespace GradeBook
{
    class Program
    {
        static void Main(string[] args)
        {
            // InMemoryBook book = new InMemoryBook("Michael");
            // book.GradeAdded += OnGradeAdded;

            IBook docBook = new DiskBook("Andrzeja");
            // docBook.AddGrade(5);
            // docBook.AddGrade(4.5);
            // docBook.AddGrade(3.5);
            // docBook.AddGrade(5);
            EnterGradesLoop(docBook); 


            // EnterGradesLoop(book);
            // Statistics stats = book.GetStatistics();
        }

        private static void EnterGradesLoop(IBook book)
        {
            while (true)
            {
                Console.WriteLine("Enter a grade or press 'q' to quit.");
                string input = Console.ReadLine();

                if (input == "q")
                {
                    book.GetStatistics();
                    break;
                }

                try
                {
                    double grade = double.Parse(input);
                    book.AddGrade(grade);
                }
                catch (ArgumentException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                catch (FormatException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    Console.WriteLine("**");
                }
            }
        }

        static void OnGradeAdded(object sender, EventArgs e)
        {
            Console.WriteLine($"A grade was added");
        }
    }
}
