using System;
using System.Collections.Generic;
using System.IO;

namespace GradeBook
{
    public class DiskBook : Book
    {
        public DiskBook(string name) : base(name)
        {
            Name = name;
            filePathFull = $"{FILES_PATH}{Name}.csv";
        }
        public override event GradeAddedDelegate GradeAdded;
        public override void AddGrade(double grade)
        {
            if (CheckIfTxtFileExists(Name))
            {
                try
                {
                    WriteGrade(grade);
                }
                catch (FileLoadException ex)
                {
                    throw ex;
                }
            }
            else
            {
                try
                {
                    File.CreateText(filePathFull);
                    WriteGrade(grade);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public override Statistics GetStatistics()
        {
            Statistics result = new Statistics();
            List<double> gradeList = GetGrades();
            result.Average = result.CalculateAverage(gradeList);
            result.High = result.FindHighestValue(gradeList);
            result.Low = result.FindLowestValue(gradeList);
            result.Letter = result.ConvertGradeToLetterGrade(result.Average);
            return result;
        }

        private void WriteGrade(double grade)
        {
            // Using forces to remove everything inside its scope after leaving that scope
            using (StreamWriter sw = File.AppendText(filePathFull))
            {
                sw.WriteLine($"{grade}");
                if (GradeAdded != null)
                {
                    GradeAdded(this, new EventArgs());
                }
            }
        }
        private void ReadGrades()
        {

        }

        private List<double> GetGrades()
        {
            List<double> grades = new List<double> { };
            if (CheckIfTxtFileExists(Name))
            {
                foreach (string line in File.ReadLines(filePathFull))
                {
                    try
                    {
                        grades.Add(double.Parse(line));
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
            else
            {
                throw new Exception("File with grades not found.");
            }
            return grades;
        }

        private bool CheckIfTxtFileExists(string fileName)
        {
            return File.Exists(filePathFull);
        }
        private static string FILES_PATH = "./files/";
        private string filePathFull;

    }
}